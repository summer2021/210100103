import queue

from skywalking.agent.protocol.grpc import GrpcProtocol
from skywalking.protocol.language_agent import Meter_pb2

if __name__ == '__main__':
    test = GrpcProtocol()
    print('ok1')
    q = queue.Queue()
    m = Meter_pb2.MeterData(singleValue=Meter_pb2.MeterSingleValue(
        name='singleValueName',
        labels=[Meter_pb2.Label(
            name='LabelName1', value='LabelValue1'),
            Meter_pb2.Label(
                name='LabelName2', value='LabelValue2')],
        value=0
    ),
        histogram=Meter_pb2.MeterHistogram(
            name='MeterHistogramName',
            labels=[Meter_pb2.Label(
                name='LabelName1', value='LabelValue1'),
                Meter_pb2.Label(
                    name='LabelName2', value='LabelValue2')],
            values=[]
        ),
        service='provider',
        serviceInstance='668d4b1ef83111ebbc3b8c85909c8b08',
        timestamp=0)
    q.put(m)

    test.report_meter(q)
    print('run ok')
