from skywalking.meter.BaseMeter import BaseMeter
from enum import Enum
from skywalking.meter.MeterId import MeterId


class Mode(Enum):
    INCREMENT = 1
    RATE = 2


class Counter(BaseMeter):
    def __init__(self, meter_id: str = '', mode: str = '', name: str = ''):
        super(BaseMeter, self).__init__(meter_id, name)
        self.mode = mode
        self.name = name

    def increment(self, count):
        self.count += 1

    def get_counter(self):
        return Counter(MeterId(), self.mode, self.name)

    """override"""

    def get_meter_type(self):
        return MeterId.MeterType.COUNTER
