from skywalking.meter.BaseMeter import BaseMeter
from skywalking.meter.BaseMeter import BaseMeter
from skywalking.meter.MeterId import MeterId


class Gauge(BaseMeter):
    """Create a counter builder by name"""
    """default value"""

    def __init__(self, meter_id, getter):
        BaseMeter.__init__(meter_id)
        self.getter = getter

    def __init__(self, name, getter):
        BaseMeter.__init__(name)
        self.getter = getter

    def get_gauge(self):
        return Gauge(MeterId(),self.getter)

    """override"""

    def get_meter_type(self):
        return MeterId.MeterType.GAUGE


